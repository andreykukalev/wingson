﻿using System;
using System.Threading.Tasks;
using Moq;
using WingsOn.Bll;
using WingsOn.Bll.Exceptions;
using WingsOn.Dal;
using WingsOn.Domain;
using Xunit;

namespace WingsOn.UnitTests
{
    public class PersonsServiceTests
    {
        private readonly Mock<IRepository<Person>> _personRepositoryMock;

        public PersonsServiceTests()
        {
            _personRepositoryMock = new Mock<IRepository<Person>>();
        }

        [Fact]
        public async Task Exists_Person_ExistingIdPassed_ReturnsPerson()
        {
            // Arrange
            var fakePersonId = 1;
            var fakePersonObj = new Person { Id = fakePersonId };

            _personRepositoryMock.Setup(x => x.Get(fakePersonId)).Returns(fakePersonObj);
            var personsService = new PersonsService(_personRepositoryMock.Object);

            // Act
            var existsResult = await personsService.ExistsAsync(fakePersonId);

            // Assert
            Assert.True(existsResult);
        }

        [Fact]
        public async Task Exists_Person_UnknownIdPassed_ReturnsPerson()
        {
            var fakePersonId = 1;

            _personRepositoryMock.Setup(x => x.Get(fakePersonId)).Returns((Person)null);
            var personsService = new PersonsService(_personRepositoryMock.Object);

            var existsResult = await personsService.ExistsAsync(fakePersonId);

            Assert.False(existsResult);
        }

        [Fact]
        public async Task Get_Person_ExistingIdPassed_ReturnsPerson()
        {
            var fakePersonId = 1;
            var fakePersonObj = new Person { Id = fakePersonId };

            _personRepositoryMock.Setup(x => x.Get(fakePersonId)).Returns(fakePersonObj);
            var personsService = new PersonsService(_personRepositoryMock.Object);

            var personResult = await personsService.GetPersonAsync(fakePersonId);

            Assert.Equal(personResult, fakePersonObj);
        }

        [Fact]
        public async Task Get_Person_UnknownIdPassed_ReturnsNull()
        {
            var fakePersonId = 1;

            _personRepositoryMock.Setup(x => x.Get(fakePersonId)).Returns((Person)null);
            var personsService = new PersonsService(_personRepositoryMock.Object);

            var personResult = await personsService.GetPersonAsync(fakePersonId);

            Assert.Null(personResult);
        }
       
        [Fact]
        public async Task Update_Person_Address_UnknownIdPassed_ThrowsNotFoundException()
        {
            var fakePersonId = 1;
            var fakeNewAddress = "new_address";

            _personRepositoryMock.Setup(x => x.Get(fakePersonId)).Returns((Person)null);
            var personsService = new PersonsService(_personRepositoryMock.Object);

            await Assert.ThrowsAsync<NotFoundException>(() => personsService.UpdateAddressAsync(fakePersonId, fakeNewAddress));
        }

        [Fact]
        public async Task Update_Person_Address_EmptyAddressPassed_ThrowsArgumentException()
        {
            var fakePersonId = 1;
            var fakeEmptyAddress = string.Empty;
            var fakePersonObj = new Person { Id = fakePersonId };

            _personRepositoryMock.Setup(x => x.Get(fakePersonId)).Returns(fakePersonObj);

            var personsService = new PersonsService(_personRepositoryMock.Object);

            await Assert.ThrowsAsync<ArgumentException>(() => personsService.UpdateAddressAsync(fakePersonId, fakeEmptyAddress));
        }

        [Fact]
        public async Task Update_Person_Address_ExistingIdWithNewAddressPassed_UpdatesAddress()
        {
            // Arrange
            var fakePersonId = 1;
            var fakeNewAddress = "new_address";
            var fakePersonObj = new Person { Id = fakePersonId };

            _personRepositoryMock.Setup(x => x.Get(fakePersonId)).Returns(fakePersonObj);
            var personsService = new PersonsService(_personRepositoryMock.Object);

            await personsService.UpdateAddressAsync(fakePersonId, fakeNewAddress);

            Assert.Equal(fakePersonObj.Address, fakeNewAddress);
        }
    }
}
