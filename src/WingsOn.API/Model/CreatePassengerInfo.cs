﻿using System.ComponentModel.DataAnnotations;
using WingsOn.Domain;

namespace WingsOn.API.Model
{
    public class CreatePassengerInfo
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string DateBirth { get; set; }

        [Required]
        public GenderType Gender { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string Email { get; set; }
    }
}
