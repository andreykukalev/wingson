﻿using System.ComponentModel.DataAnnotations;

namespace WingsOn.API.Model
{
    public class UpdatePassengerInfo
    {
        [Required]
        public string Address { get; set; }
    }
}
