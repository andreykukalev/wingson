﻿using System;
using System.Globalization;
using AutoMapper;
using WingsOn.API.Model;
using WingsOn.Domain;

namespace WingsOn.API.AutoMapper
{
    public class MapperConfig: Profile
    {
        public MapperConfig()
        {
            CultureInfo cultureInfo = new CultureInfo("nl-NL");

            CreateMap<CreatePassengerInfo, Person>()
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.Name))
                .ForMember(x => x.Email, opt => opt.MapFrom(x => x.Email))
                .ForMember(x => x.Address, opt => opt.MapFrom(x => x.Address))
                .ForMember(x => x.DateBirth,
                    opt => opt.MapFrom((x, _) =>
                        DateTime.TryParse(x.DateBirth, out var dateTime) ? dateTime : DateTime.MinValue))
                .ForMember(x => x.Gender, opt => opt.MapFrom(x => x.Gender));
        }
    }
}
