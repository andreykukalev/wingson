﻿using WingsOn.Domain;

namespace WingsOn.API.Model
{
    public class PassengersFilter
    {
        public GenderType? Gender { get; set; }

        public int? Portion { get; set; }

        public int? Page { get; set; }
    }
}
