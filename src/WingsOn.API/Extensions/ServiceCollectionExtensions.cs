﻿using Microsoft.Extensions.DependencyInjection;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.API.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInMemoryRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IRepository<Person>, PersonRepository>();
            services.AddSingleton<IRepository<Flight>, FlightRepository>();
            services.AddSingleton<IRepository<Booking>, BookingRepository>();

            services.AddSingleton<IRepository<Airport>, AirportRepository>();
            services.AddSingleton<IRepository<Airline>, AirlineRepository>();

            return services;
        }
    }
}
