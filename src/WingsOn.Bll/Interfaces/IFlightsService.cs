﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WingsOn.Domain;

namespace WingsOn.Bll.Interfaces
{
    public interface IFlightsService
    {
        Task<bool> ExistsAsync(string flight);

        Task<Person[]> GetPassengersAsync(string flight);

        Task<Person[]> GetPassengersAsync(string flight, GenderType gender);

        Task CreatePassengerAsync(string flight, Person personObj);
    }
}
