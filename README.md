## Narrative:
As a Front End Developer I need a REST-ful Web API for my ticketing website, so that I can access and
manage information related to the passenger.

---

## Acceptance criteria:

We need the following resources for our website:

1. Endpoint that returns a Person by Id.
2. Endpoint that returns all passengers on the flight ‘PZ696’.
3. Endpoint that lists all the male passengers.
4. BONUS: Endpoint that creates a booking of an existing flight for a new passenger.
5. BONUS: Endpoint that updates passenger’s address.

---

## Recommendations:
1. Assume you are implementing a production-ready solution.
2. Focus on code quality and coverage.
3. Put your best on it, but keep it simple.
4. Extending the provided code is optional, but changing it is not recommended.