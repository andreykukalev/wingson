﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WingsOn.Bll.Exceptions;
using WingsOn.Bll.Interfaces;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Bll
{
    public class FlightsService: IFlightsService
    {
        private readonly IRepository<Person> _personRepository;
        private readonly IRepository<Flight> _flightRepository;
        private readonly IRepository<Booking> _bookingRepository;

        public FlightsService(
            IRepository<Person> personRepository,
            IRepository<Flight> flightRepository,
            IRepository<Booking> bookingRepository)
        {
            _personRepository = personRepository;
            _flightRepository = flightRepository;
            _bookingRepository = bookingRepository;
        }

        public async Task<bool> ExistsAsync(string flight)
        {
            if (string.IsNullOrEmpty(flight))
                return false;

            var flightObj = await Task.FromResult(_flightRepository.GetAll().FirstOrDefault(item => item.Number.Equals(flight, StringComparison.CurrentCultureIgnoreCase)));

            if (flightObj == null)
                return false;

            return true;
        }

        public async Task<Person[]> GetPassengersAsync(string flight)
        {
            if (string.IsNullOrEmpty(flight))
                return new Person[0];

            var bookings = await Task.FromResult(_bookingRepository.GetAll().Where(item=>item.Flight.Number.Equals(flight, StringComparison.CurrentCultureIgnoreCase)).ToArray());

            if (bookings == null || !bookings.Any())
                return new Person[0];

            return bookings.SelectMany(item => item.Passengers).ToArray();
        }

        public async Task<Person[]> GetPassengersAsync(string flight, GenderType gender)
        {
            if (string.IsNullOrEmpty(flight))
                return new Person[0];

            var passengers = await GetPassengersAsync(flight);

            return passengers?.Where(item => item.Gender == gender).ToArray();
        }

        public async Task CreatePassengerAsync(string flight, Person personObj)
        {
            if (string.IsNullOrEmpty(flight))
                throw new ArgumentException("Parameter cannot be null or empty", nameof(flight));

            if (personObj == null)
                throw new ArgumentException("Parameter cannot be null", nameof(personObj));

            //Since in repo there are several flights under the same number and logic of selection not clear - just select first item.
            var flightObj = (await Task.FromResult(_flightRepository.GetAll())).FirstOrDefault(item => item.Number.Equals(flight, StringComparison.CurrentCultureIgnoreCase));

            if (flightObj == null)
                throw new NotFoundException($"Flight with Number:{flight} Not Found");
            
#if DEBUG //Simple workaround for generating unique ID
            personObj.Id = _personRepository.GetAll().Any() ? _personRepository.GetAll().Max(item => item.Id) + 1 : 1;
#endif
            var bookingObj = new Booking()
            {
#if DEBUG //Simple workaround for generating unique ID
                Id = _bookingRepository.GetAll().Any() ? _bookingRepository.GetAll().Max(item=>item.Id) + 1 : 1,
#endif
                Number = GenerateBookingNumber(),
                DateBooking = DateTime.Now,
                Customer = personObj,
                Flight = flightObj,
                //Just add person in Passengers list until logic behind the list is clarified
                Passengers = new List<Person>(){ personObj }
            };

            _personRepository.Save(personObj);
            _bookingRepository.Save(bookingObj);
        }

        #region Helpers
        
        private string GenerateBookingNumber()
        {
            return $"{RandomString(2)}-{RandomNumber(100000, 999999)}";
        }
        
        private int RandomNumber(int min, int max)    
        {    
            var random = new Random();    
            return random.Next(min, max);    
        }    
        
        private string RandomString(int size)    
        {    
            var builder = new StringBuilder();    
            var random = new Random();

            for (int i = 0; i < size; i++)
            {
                var ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(char.ToUpper(ch));
            }         
            return builder.ToString();    
        }

        #endregion
    }
}
