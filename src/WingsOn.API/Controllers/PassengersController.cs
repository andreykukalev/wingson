﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WingsOn.API.Model;
using WingsOn.API.Model.Response;
using WingsOn.Bll.Interfaces;
using WingsOn.Domain;

namespace WingsOn.API.Controllers
{
    /// <summary>
    /// This controller does handling Passengers requests.
    /// Uses constructions like "await Task.FromResult(...)" in order to emulate async calls
    /// </summary>
    public class PassengersController : ControllerBase
    {
        private readonly IFlightsService _flightsService;
        private readonly IMapper _mapper;

        public PassengersController(
            IFlightsService flightsService,
            IMapper mapper)
        {
            _flightsService = flightsService;
            _mapper = mapper;
        }

        
        // GET api/flights/PZ696/passengers[?gender=male&page=1&portion=5]
        // GET api/passengers[?flight=PZ696&gender=male&page=1&portion=5]
        [HttpGet()]
        [Route("api/[Controller]")]
        [Route("api/flights/{flight}/[Controller]")]
        public async Task<ActionResult> GetPassengers(string flight, [FromQuery] PassengersFilter filter)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            if(string.IsNullOrEmpty(flight) || !await _flightsService.ExistsAsync(flight))
                return NotFound(new NotFoundObjectResult($"Flight with Number:{flight} Not Found"));

            var response = new CollectionResponse<Person>();
            
            Person[] passengers;

            if (filter?.Gender == null)
                passengers = await _flightsService.GetPassengersAsync(flight);
            else
                passengers = await _flightsService.GetPassengersAsync(flight, filter.Gender.Value);

            if (filter?.Page != null && filter?.Portion != null)
            {
                response.Items = passengers
                    .Skip((filter.Page.Value - 1) * filter.Portion.Value)
                    .Take(filter.Portion.Value)
                    .Select(item => item);
            }

            response.Items = passengers;

            Response.Headers.Add("X-Total-Count", response.Items.Count().ToString());

            return Ok(response);
        }

        // POST api/flights/PZ696/Passengers
        [HttpPost()]
        [Route("api/flights/{flight}/[Controller]")]
        public async Task<ActionResult> PostPassenger([FromRoute] string flight, [FromBody] CreatePassengerInfo passengerInfo)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if(string.IsNullOrEmpty(flight) || !await _flightsService.ExistsAsync(flight))
                return NotFound(new NotFoundObjectResult($"Flight with Number:{flight} Not Found"));

            var personObj = new Person();

            personObj = _mapper.Map(passengerInfo, personObj);

            await _flightsService.CreatePassengerAsync(flight, personObj);
            
            return CreatedAtAction("PostPassenger", new { id = personObj.Id }, personObj);
        }
    }
}
