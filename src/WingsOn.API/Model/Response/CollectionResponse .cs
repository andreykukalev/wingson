﻿using System;
using System.Collections.Generic;

namespace WingsOn.API.Model.Response
{
    public class CollectionResponse<T> where T: class
    {
        public IEnumerable<T> Items { get; set; }
    }
}
