﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WingsOn.Domain;

namespace WingsOn.Bll.Interfaces
{
    public interface IPersonsService
    {
        Task<bool> ExistsAsync(int id);

        Task<Person> GetPersonAsync(int id);

        Task UpdateAddressAsync(int id, string newAddress);
    }
}
