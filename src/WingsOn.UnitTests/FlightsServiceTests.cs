using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using WingsOn.Bll;
using WingsOn.Bll.Exceptions;
using WingsOn.Dal;
using WingsOn.Domain;
using Xunit;

namespace WingsOn.UnitTests
{
    public class FlightsServiceTests
    {
        private readonly Mock<IRepository<Person>> _personRepositoryMock;
        private readonly Mock<IRepository<Flight>> _flightRepositoryMock;
        private readonly Mock<IRepository<Booking>> _bookingRepositoryMock;

        public FlightsServiceTests()
        {
            _personRepositoryMock = new Mock<IRepository<Person>>();
            _flightRepositoryMock = new Mock<IRepository<Flight>>();
            _bookingRepositoryMock = new Mock<IRepository<Booking>>();
        }

        [Fact]
        public async Task Exists_Flight_ExistingFlightPassed_ReturnsPerson()
        {
            // Arrange
            var fakeFlightNumber = "ABC";
            var fakeFlightsCollection = new []
            {
                new Flight() {Number = fakeFlightNumber}
            };

            _flightRepositoryMock.Setup(x => x.GetAll()).Returns(fakeFlightsCollection);

            var flightService = new FlightsService(
                _personRepositoryMock.Object,
                _flightRepositoryMock.Object,
                _bookingRepositoryMock.Object);

            // Act
            var existsResult = await flightService.ExistsAsync(fakeFlightNumber);

            // Assert
            Assert.True(existsResult);
        }

        [Fact]
        public async Task Get_Passengers_UnknownFlightPassed_ReturnsEmptyCollection()
        {
            var fakeBookings = GetFakeBookings().ToArray();

            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(fakeBookings);

            var flightService = new FlightsService(
                _personRepositoryMock.Object,
                _flightRepositoryMock.Object,
                _bookingRepositoryMock.Object);

            var personsResult = await flightService.GetPassengersAsync(string.Empty);

            Assert.Empty(personsResult);
        }

        [Fact]
        public async Task Get_Passengers_ExistingFlightPassed_ReturnsPersonCollection()
        {
            var flightNumber = "PZ696";
            var fakeBookings = GetFakeBookings().ToArray();

            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(fakeBookings);

            var flightService = new FlightsService(
                _personRepositoryMock.Object,
                _flightRepositoryMock.Object,
                _bookingRepositoryMock.Object);

            var personsResult = await flightService.GetPassengersAsync(flightNumber);
            var expectedResult = fakeBookings
                .Where(item=>item.Flight.Number.Equals(flightNumber, StringComparison.CurrentCultureIgnoreCase))
                .SelectMany(item=>item.Passengers);

            Assert.NotEmpty(personsResult);
            Assert.Equal(personsResult, expectedResult);
        }

        [Fact]
        public async Task Get_Passengers_ExistingFlightAndGenderFilterPassed_ReturnsFilteredPersonCollection()
        {
            var flightNumber = "PZ696";
            var gender = GenderType.Male;
            var fakeBookings = GetFakeBookings().ToArray();

            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(fakeBookings);

            var flightService = new FlightsService(
                _personRepositoryMock.Object,
                _flightRepositoryMock.Object,
                _bookingRepositoryMock.Object);

            var personsResult = await flightService.GetPassengersAsync(flightNumber, gender);

            Assert.NotEmpty(personsResult);
            Assert.All(personsResult, person=>Assert.Equal(person.Gender, gender));
        }

        [Fact]
        public async Task Post_Passenger_UnknownFlightPassed_ThrowsNotFoundException()
        {
            var flightNumber = "11111";

            var flightService = new FlightsService(
                _personRepositoryMock.Object,
                _flightRepositoryMock.Object,
                _bookingRepositoryMock.Object);

            await Assert.ThrowsAsync<NotFoundException>(() => flightService.CreatePassengerAsync(flightNumber, new Person()));
        }

        [Fact]
        public async Task Post_Passenger_ExistingFlightPassed_()
        {
            var flightNumber = "PZ696";

            var fakePersonId = 1;
            var fakePersonObj = new Person { Id = fakePersonId };

            var fakeBookings = GetFakeBookings();
            var fakeFlights = GetFakeFlights();
            
            _bookingRepositoryMock.Setup(x => x.GetAll()).Returns(fakeBookings);
            _flightRepositoryMock.Setup(x => x.GetAll()).Returns(fakeFlights);

            var flightService = new FlightsService(
                _personRepositoryMock.Object,
                _flightRepositoryMock.Object,
                _bookingRepositoryMock.Object);

            await flightService.CreatePassengerAsync(flightNumber, fakePersonObj);

            _personRepositoryMock.Verify(mock=>mock.Save(fakePersonObj), Times.Once);
            _bookingRepositoryMock.Verify(mock=>mock.Save(It.IsAny<Booking>()), Times.Once);
        }
        
        #region Helpers

        private Booking[] GetFakeBookings()
        {
            //In order to simplify, will reuse existed repos
            var bookingRepository = new BookingRepository(
                new PersonRepository(),
                new FlightRepository(
                    new AirportRepository(),
                    new AirlineRepository()));

            return bookingRepository.GetAll().ToArray();
        }

        private Flight[] GetFakeFlights()
        {
            //In order to simplify, will reuse existed repos
            var flightRepository = new FlightRepository(new AirportRepository(), new AirlineRepository());

            return flightRepository.GetAll().ToArray();
        }

        #endregion
    }
}
