﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WingsOn.Bll.Exceptions;
using WingsOn.Bll.Interfaces;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.Bll
{
    public class PersonsService: IPersonsService
    {
        private readonly IRepository<Person> _personRepository;

        public PersonsService(IRepository<Person> personRepository)
        {
            _personRepository = personRepository;
        }

        public async Task<bool> ExistsAsync(int id)
        {
            return await GetPersonAsync(id) != null;
        }

        public Task<Person> GetPersonAsync(int id)
        {
            return Task.FromResult(_personRepository.Get(id));
        }

        public async Task UpdateAddressAsync(int id, string newAddress)
        {
            if (string.IsNullOrEmpty(newAddress))
                throw new ArgumentException("Parameter cannot be null or empty", nameof(newAddress));

            var personObj = await GetPersonAsync(id);

            if (personObj == null)
                throw new NotFoundException($"Person with ID:{id} Not Found");

            personObj.Address = newAddress;

            _personRepository.Save(personObj);
        }
    }
}
