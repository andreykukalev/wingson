using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using WingsOn.API;
using WingsOn.API.Model;
using WingsOn.API.Model.Response;
using WingsOn.Domain;
using Xunit;

namespace WingsOn.IntegrationTests
{
    public class PassengersTests
    {
        #region Consts

        const string ExistingFlightNumber = "PZ696";

        const string UnknownFlightNumber = "AABBC";

        #endregion

        private readonly HttpClient _client;

        public PassengersTests()
        {
            var server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = server.CreateClient();
        }

        [Theory]
        [InlineData(UnknownFlightNumber)]
        public async Task Get_Passengers_UnknownFlightNumber_ReturnsOkResult(string unknownFlightNumber)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"api/flights/{unknownFlightNumber}/passengers");

            var response = await _client.SendAsync(request);
    
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Theory]
        [InlineData(ExistingFlightNumber, GenderType.Male) ]
        [InlineData(ExistingFlightNumber, GenderType.Female) ]
        public async Task Get_Passengers_ExistingFlightNumberAndGenderFilter_ReturnsOkResult(string existingFlightNumber, GenderType gender)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"api/flights/{existingFlightNumber}/passengers?gender={gender.ToString()}");

            var response = await _client.SendAsync(request);
            var content = await response.Content.ReadAsStringAsync();
            var persons = JsonConvert.DeserializeObject<CollectionResponse<Person>>(content);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True(IsJsonValid(content, GetPersonsCollectionJsonSchema()));
            Assert.True(persons.Items.All(item=>item.Gender == gender));
            Assert.True(response.Headers.Contains("X-Total-Count"));
            Assert.Equal(response.Headers.GetValues("X-Total-Count").FirstOrDefault(), persons.Items.Count().ToString());
        }
        
        [Theory]
        [InlineData(UnknownFlightNumber)]
        public async Task Post_Passenger_UnknownFlightNumber_ReturnsNotFoundResponse(string unknownFlightNumber)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, $"api/flights/{unknownFlightNumber}/passengers")
            {
                Content = new StringContent(JsonConvert.SerializeObject(GetPassengerInfo()), Encoding.UTF8, "application/json")
            };

            var response = await _client.SendAsync(request);

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Theory]
        [InlineData("")]
        public async Task Post_Passenger_EmptyFlightNumber_ReturnsNotFoundResponse(string emptyFlightNumber)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, $"api/flights/{emptyFlightNumber}/passengers")
            {
                Content = new StringContent(JsonConvert.SerializeObject(GetPassengerInfo()), Encoding.UTF8, "application/json")
            };

            var response = await _client.SendAsync(request);

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Theory]
        [InlineData(ExistingFlightNumber)]
        public async Task Post_Passenger_ExistingFlightNumberAndPassengerInfoPassed_ReturnsCreatedResponse(string existingFlightNumber)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, $"api/flights/{existingFlightNumber}/passengers")
            {
                Content = new StringContent(JsonConvert.SerializeObject(GetPassengerInfo()), Encoding.UTF8, "application/json")
            };

            var response = await _client.SendAsync(request);
            var content = await response.Content.ReadAsStringAsync();

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
            Assert.True(IsJsonValid(content, GetPersonJsonSchema()));
        }

        [Theory]
        [InlineData(ExistingFlightNumber)]
        public async Task Post_Passenger_ExistingFlightNumberAndInvalidPassengerInfoPassed_ReturnsBadRequestResponse(string existingFlightNumber)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, $"api/flights/{existingFlightNumber}/passengers")
            {
                Content = new StringContent(JsonConvert.SerializeObject(new { Text="Invalid Passenger Info" }), Encoding.UTF8, "application/json")
            };

            var response = await _client.SendAsync(request);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        #region Helpers

        private bool IsJsonValid(string json, string schema)
        {
            var jsonSchema = JSchema.Parse(schema);
            var jsonObject = JObject.Parse(json);
            return jsonObject.IsValid(jsonSchema);
        }

        private string GetPersonJsonSchema()
        {
            var jSchema = new JSchema
            {
                Type = JSchemaType.Object,
                Properties =
                {
                    { "name", new JSchema { Type = JSchemaType.String } },
                    { "dateBirth", new JSchema { Type = JSchemaType.String } },
                    { "gender", new JSchema { Type = JSchemaType.Integer } },
                    { "address", new JSchema { Type = JSchemaType.String } },
                    { "email", new JSchema { Type = JSchemaType.String } },
                    { "id", new JSchema { Type = JSchemaType.Integer } }
                }
            };

            return jSchema.ToString();
        }
        private string GetPersonsCollectionJsonSchema()
        {
            var jSchema = new JSchema
            {
                Type = JSchemaType.Object,
                Properties =
                {
                    {
                        "Items", new JSchema
                        {
                            Type = JSchemaType.Array,
                            Properties =
                            {
                                {"name", new JSchema {Type = JSchemaType.String}},
                                {"dateBirth", new JSchema {Type = JSchemaType.String}},
                                {"gender", new JSchema {Type = JSchemaType.Integer}},
                                {"address", new JSchema {Type = JSchemaType.String}},
                                {"email", new JSchema {Type = JSchemaType.String}},
                                {"id", new JSchema {Type = JSchemaType.Integer}}
                            }
                        }

                    }
                }
            };

            return jSchema.ToString();
        }

        private CreatePassengerInfo GetPassengerInfo()
        {
            return new CreatePassengerInfo()
            {
                Name = "Test Name",
                Address = "Test Address",
                DateBirth = "1990-03-28T00:00:00",
                Email = "test@email.com",
                Gender = GenderType.Male
            };
        }

        #endregion
    }
}
