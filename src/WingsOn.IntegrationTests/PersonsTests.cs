﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using WingsOn.API;
using WingsOn.API.Model;
using WingsOn.Domain;
using Xunit;

namespace WingsOn.IntegrationTests
{
    public class PersonsTests
    {
        private readonly HttpClient _client;

        public PersonsTests()
        {
            var server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = server.CreateClient();
        }
        
        [Theory]
        [InlineData(1)]
        [InlineData(100)]
        public async Task Get_Person_UnknownIdPassed_ReturnsNotFoundResult(int id)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"api/persons/{id}");

            var response = await _client.SendAsync(request);

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Theory]
        [InlineData(69)]
        [InlineData(91)]
        public async Task Get_Person_ExistingIdPassed_ReturnsOkResult(int id)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"api/persons/{id}");

            var response = await _client.SendAsync(request);
            var content = await response.Content.ReadAsStringAsync();
    
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True(IsJsonValid(content, GetPersonJsonSchema()));
        }

        [Theory]
        [InlineData(1)]
        [InlineData(100)]
        public async Task Patch_Person_UnknownIdPassed_ReturnsNotFoundResult(int id)
        {
            var request = new HttpRequestMessage(HttpMethod.Patch, $"api/persons/{id}")
            {
                Content = new StringContent(JsonConvert.SerializeObject(GetPassengerInfo()), Encoding.UTF8, "application/json")
            };

            var response = await _client.SendAsync(request);

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Theory]
        [InlineData(69)]
        [InlineData(91)]
        public async Task Patch_Person_ExistingIdAndNewEmailPassed_ReturnsNoContentResult(int id)
        {
            var request = new HttpRequestMessage(HttpMethod.Patch, $"api/persons/{id}")
            {
                Content = new StringContent(JsonConvert.SerializeObject(GetPassengerInfo()), Encoding.UTF8, "application/json")
            };

            var response = await _client.SendAsync(request);

            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }

         #region Helpers

        private bool IsJsonValid(string json, string schema)
        {
            var jsonSchema = JSchema.Parse(schema);
            var jsonObject = JObject.Parse(json);
            return jsonObject.IsValid(jsonSchema);
        }

        private string GetPersonJsonSchema()
        {
            var jSchema = new JSchema
            {
                Type = JSchemaType.Object,
                Properties =
                {
                    { "name", new JSchema { Type = JSchemaType.String } },
                    { "dateBirth", new JSchema { Type = JSchemaType.String } },
                    { "gender", new JSchema { Type = JSchemaType.Integer } },
                    { "address", new JSchema { Type = JSchemaType.String } },
                    { "email", new JSchema { Type = JSchemaType.String } },
                    { "id", new JSchema { Type = JSchemaType.Integer } }
                }
            };

            return jSchema.ToString();
        }

        private CreatePassengerInfo GetPassengerInfo()
        {
            return new CreatePassengerInfo()
            {
                Name = "Test Name",
                Address = "Test Address",
                DateBirth = "1990-03-28T00:00:00",
                Email = "test@email.com",
                Gender = GenderType.Male
            };
        }

        #endregion
    }
}
