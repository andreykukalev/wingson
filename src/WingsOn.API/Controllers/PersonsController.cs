﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WingsOn.API.Model;
using WingsOn.Bll.Interfaces;
using WingsOn.Dal;
using WingsOn.Domain;

namespace WingsOn.API.Controllers
{
    [Route("api/[Controller]")]
    public class PersonsController : ControllerBase
    {
        private readonly IPersonsService _personsService;
        private readonly IMapper _mapper;

        public PersonsController(
            IPersonsService personsService,
            IMapper mapper)
        {
            _personsService = personsService;
            _mapper = mapper;
        }

        // GET api/passengers/id
        [HttpGet("{id}")]
        public async Task<ActionResult> GetPerson([FromRoute] int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var personObj = await _personsService.GetPersonAsync(id);

            if (personObj == null)
                return NotFound(new NotFoundObjectResult($"Person with ID:{id} Not Found"));

            return Ok(personObj);
        }

        // PATCH api/passengers/id
        [HttpPatch("{id}")]
        public async Task<ActionResult> PatchPerson([FromRoute] int id, [FromBody] UpdatePassengerInfo passengerInfo)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            if (!await _personsService.ExistsAsync(id))
                return NotFound(new NotFoundObjectResult($"Person with ID:{id} Not Found"));

            await _personsService.UpdateAddressAsync(id, passengerInfo.Address);

            return NoContent();
        }
    }
}
