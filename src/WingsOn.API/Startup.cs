using System;
using System.Net;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WingsOn.Dal;
using WingsOn.Domain;
using AutoMapper;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WingsOn.Bll;
using WingsOn.Bll.Interfaces;
using WingsOn.API.Extensions;
using WingsOn.Bll.Exceptions;

namespace WingsOn.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddInMemoryRepositories();

            services.AddScoped<IFlightsService, FlightsService>();
            services.AddScoped<IPersonsService, PersonsService>();

            services.AddAutoMapper((provider, config) =>
            {
                config.ConstructServicesUsing(provider.GetService);
            }, AppDomain.CurrentDomain.GetAssemblies());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory logger)
        {
            var log = logger.CreateLogger<Startup>();
            //Simple middleware for sending error code to client in case of any troubles on backend.
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    var statusCode = (int) HttpStatusCode.InternalServerError;

                    context.Response.StatusCode = statusCode;
                    context.Response.ContentType = "application/json";

                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    if (contextFeature != null)
                    {
                        ObjectResult result;

                        if (contextFeature.Error is NotFoundException)
                        {
                            log.LogWarning("NotFoundException Is Raised!", contextFeature.Error);
                            result = new NotFoundObjectResult(contextFeature.Error.Message);
                        }
                        else
                        {
                            log.LogError("Internal Error", contextFeature.Error);
                            result = new ObjectResult("Internal Server Error")
                            {
                                StatusCode = statusCode
                            };
                        }

                        await context.Response.WriteAsync(JsonConvert.SerializeObject(result).ToString());
                    }
                });
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
